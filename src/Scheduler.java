package PT2018.Tema2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Scheduler {
	public List<Server> servers;
	public ArrayList<Thread> arrayThread=new ArrayList<Thread>();
	public int maxNoServers;
	public int maxTasksperServer = 10;

	public Scheduler(int maxNoServers) {
		servers = new ArrayList<Server>();
		for (int i = 0; i < maxNoServers; i++) {
			// String a="s"+i;
			Server s = new Server();
			servers.add(s);
			Thread t = new Thread(s);
			arrayThread.add(t);

		}
	}
	/*
//dispatch fara iterator
	public void dispatchTask(Task t) {
		//Iterator<Server> it = servers.iterator();
		int timpMinim = 1000;
		int i=0;
		Server freeServer = new Server();
		while ( i<servers.size()) {
			Server casa =servers.get(i);
			i++;
			if (casa.getWaitingPeriod().intValue() < timpMinim) {
				timpMinim = casa.getWaitingPeriod().intValue();
				freeServer = casa;
			}
		}
		freeServer.addTask(t);
	//freeServer.run();
		
	}
	*/

	
	public synchronized void dispatchTask(Task t) {
		Iterator<Server> it = servers.iterator();
		int timpMinim = 1000;
		Server freeServer = new Server();
		while (it.hasNext()) {
			Server casa = it.next();
			if (casa.getWaitingPeriod().intValue() < timpMinim) {
				timpMinim = casa.getWaitingPeriod().intValue();
				freeServer = casa;
			}
		}
		freeServer.addTask(t);
	//freeServer.run();
		
	}
	
	


	@Override
	public String toString() {
		return "[" + servers +  "]";
	}

	public List<Server> getServers() {
		return servers;
	}
}
