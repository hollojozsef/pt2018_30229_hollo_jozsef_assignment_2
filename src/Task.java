package PT2018.Tema2;

public class Task {
	public int arrivalTime;
	public int processingTime;

	public int getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public int getProcessingTime() {
		return processingTime;
	}

	public void setProcessingTime(int processingTime) {
		this.processingTime = processingTime;
	}

	public Task(int arrivalTime, int processingTime) {
		this.arrivalTime = arrivalTime;
		this.processingTime = processingTime;
	}

	public void decrementTime() {
		this.processingTime--;
	}
	
	@Override
	public String toString() {
		return "Task [AT=" + arrivalTime + ", PT=" + processingTime + "]";
	}

}
