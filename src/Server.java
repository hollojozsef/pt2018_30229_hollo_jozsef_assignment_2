package PT2018.Tema2;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.*;

public class Server implements Runnable {
	public BlockingQueue<Task> tasks;
	public AtomicInteger waitingPeriod = new AtomicInteger();

	public Server() {
		this.tasks = new ArrayBlockingQueue<Task>(10);
		this.waitingPeriod.set(0);
	}

	public synchronized void addTask(Task newTask) {
		this.waitingPeriod.addAndGet(newTask.processingTime);
		try {
			this.tasks.put(newTask);				////pus put in loc de add
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void removeTask() {
		System.out.println("mere remove");
		try {
			tasks.take();
		} catch (InterruptedException e) {
			System.out.println("remove error");
		}
	}
	
	public void decrementWaitingPeriod() {
		this.waitingPeriod.decrementAndGet();
	}
	
	@Override
	public String toString() {
		return "Server [tasks=" + tasks + ", waitingPeriod=" + waitingPeriod + "]";
	}

	public void run() {
		while (true) {
			try {
				Task t = this.tasks.take();
				this.waitingPeriod.decrementAndGet();
				
				//Thread.sleep( 1000);
								//					System.out.println(waitingPeriod.get());
				//this.waitingPeriod.getAndDecrement();
				
			} catch (Exception e) {
				System.out.println("server error");
			}
		}
	}

	public void setWaitingPeriod(AtomicInteger waitingPeriod) {
		this.waitingPeriod = waitingPeriod;
	}

/*	public Task[] getTasks() {
		Iterator<Task> it = tasks.iterator();
		int i = 0;
		Task[] t = new Task[10];
		while (it.hasNext()) {
			t[i] = it.next();
		}
		return t;
	}*/
	public Task[] getTasks() {
		Task[] t = new Task[10];
		//try {
		
		System.out.println("get Task");
	for (int i =0 ;i<tasks.size();i++) {
			t[i]=tasks.peek();
	}
	
		//Thread.sleep(1000);
		//notify();
		//} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		//e.printStackTrace();
	//}
		return t;
	
}
	public AtomicInteger getWaitingPeriod() {
		return waitingPeriod;
	}

}
