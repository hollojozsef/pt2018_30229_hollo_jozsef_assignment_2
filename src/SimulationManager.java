package Controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import PT2018.Tema2.Scheduler;
import PT2018.Tema2.Task;
import View.SimulatorFrame;

public class SimulationManager implements Runnable {
	public int timeLimit ;//= 30;
	public int maxProcessingTime ;//= 10;
	public int minProcessingTime ;//= 2;
	public int numberOfServers ;//= 3;
	public int numberofClients ;//= 10;
	public Scheduler scheduler;
	public List<Task> generatedTask=new ArrayList<Task>();
	SimulatorFrame a;///=new SimulatorFrame();

	public SimulationManager(SimulatorFrame a,int timeLimit,int minProcessingTime,int maxProcessingTime,int numberOfServers,int numberofClients) {
		scheduler = new Scheduler(numberOfServers);
		this.timeLimit=timeLimit;
		this.minProcessingTime=minProcessingTime;
		this.maxProcessingTime=maxProcessingTime;
		this.numberOfServers=numberOfServers;
		this.numberofClients=numberofClients;
		this.a=a;
	}

	public void generateNRandomTasks() {
		for (int i = 0; i < numberofClients; i++) {
			Random rand = new Random();
			int randomProcessingTime = rand.nextInt((maxProcessingTime - minProcessingTime) + 1) + minProcessingTime;
			Random rand2 = new Random();
			int timpSosire = rand2.nextInt(timeLimit);
			Task t = new Task(timpSosire, randomProcessingTime);
			System.out.println(t.toString());
			generatedTask.add(t);
		}
		for (int i = 0; i < generatedTask.size(); i++) {
			for (int j = i + 1; j < generatedTask.size(); j++) {
				if (generatedTask.get(i).getArrivalTime() > generatedTask.get(j).getArrivalTime()) {
					Collections.swap(generatedTask, i, j);
				}
			}
		}

	}
	int currentTime = 0;
	public void run() {
		
		int i=0;
		while (currentTime < timeLimit) {
			
			try {
				Task currentTask=new Task(0,0);		
				for(Iterator<Task> it=generatedTask.iterator();it.hasNext();) {
					 currentTask = it.next();
					i++;
					
					if (currentTask.getArrivalTime() == currentTime) {
						scheduler.dispatchTask(currentTask);
						generatedTask.remove(currentTask);
						scheduler.arrayThread.get(i).start();
						
					}
				//	if(scheduler.servers.get(i).getWaitingPeriod().get()>0) {
					//	scheduler.servers.get(i).decrementWaitingPeriod();
				///	}
					//scheduler.servers.get(i).decrementWaitingPeriod();
					//if(currentTask.getArrivalTime()+currentTask.getProcessingTime()==currentTime) {
						
					//}

				}
			
				//	if(scheduler.servers.get(j).getTasks()[j].getProcessingTime()+scheduler.servers.get(j).getTasks()[j].getArrivalTime()==currentTime)
						//scheduler.servers.get(k).removeTask();
					//	scheduler.servers.get(j).tasks.remove();
				//if(	scheduler.servers.get(j).tasks.peek().getProcessingTime()+scheduler.servers.get(j).tasks.peek().getArrivalTime()==currentTime) {
					//scheduler.servers.get(j).tasks.remove();
				//	System.out.println("remove");
				//}
				
				//for(int k=0;k<3;k++) {
				//System.out.println("intra in for la remove" + k);
				//if(scheduler.servers.get(k).getTasks()[k].getProcessingTime()+scheduler.servers.get(k).getTasks()[k].getArrivalTime()==currentTime)
				//	//scheduler.servers.get(k).removeTask();
				//	scheduler.servers.get(k).tasks.remove();
				//}
				for(int j=0;j<numberOfServers;j++) {
					if(scheduler.servers.get(j).getWaitingPeriod().intValue()>0)
					scheduler.servers.get(j).decrementWaitingPeriod();
				}
				
				
				currentTime++;
			
			a.area.append(currentTime+" "+scheduler.toString()+"\n");
				Thread.sleep(1000);
			
			} catch (Exception e) {
			}
			try {
				
			
				for(int j=0;j<numberOfServers;j++) {
					
							if(scheduler.servers.get(j).getWaitingPeriod().intValue()==0&&scheduler.servers.get(j).tasks!=null) {
							
								scheduler.servers.get(j).tasks.clear();
								
						}
							
						
							if(	scheduler.servers.get(j).tasks.peek().getProcessingTime()+scheduler.servers.get(j).tasks.peek().getArrivalTime()==currentTime) {
								//System.out.println(scheduler.servers.get(j).tasks.poll().toString());
								scheduler.servers.get(j).tasks.poll();
						
						}
						}
				
				
	
			Thread.sleep(1000);
			}
			catch (Exception e) {
			}
			
		}

	}
}

