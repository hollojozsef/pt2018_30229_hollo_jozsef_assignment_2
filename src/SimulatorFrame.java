package View;



import javax.swing.JFrame;

import Controller.SimulationManager;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class SimulatorFrame implements ActionListener{
	JLabel l1,l2;  
	public JTextArea area;  
	JButton b;  
	  JTextField limTimp,timpMin,timpMax,nrSer,nrCL;
	public SimulatorFrame() {  
	    JFrame f= new JFrame();  
	    l1=new JLabel();  
	    l1.setBounds(50,25,100,30);  
	    l2=new JLabel();  
	    l2.setBounds(160,25,100,30);  
	    area=new JTextArea();  
	    area.setBounds(50,50,800,400);  
	    b=new JButton("START");  
	    b.setBounds(500,450,120,30);  
	    b.addActionListener(this);  
	    f.add(l1);f.add(l2);f.add(b);  
	    f.setSize(1000,600); 
	    area.setLineWrap(true);
	    JScrollPane scrollPane = new JScrollPane(area,ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
	    f.add(area);
	    f.setLayout(null);  
	    f.setVisible(true);  
	    JLabel limitaTimp, timpMinimProcesare, timpMaximProcesare, nrServeri, nrClienti;
	    
	    limitaTimp=new JLabel("Limita Timp");
	    limitaTimp.setBounds(860,30,70,30);
	    limitaTimp.setVisible(true);
	    f.add(limitaTimp);
	    
	    limTimp=new JTextField();
	    limTimp.setBounds(860,60,70,30);
	    limTimp.setVisible(true);
	    f.add(limTimp);
	    
	    timpMinimProcesare=new JLabel("Timp minim:");
	    timpMinimProcesare.setBounds(860,100,70,30);
	    timpMinimProcesare.setVisible(true);
	    f.add(timpMinimProcesare);
	    
	    timpMin=new JTextField();
	    timpMin.setBounds(860,140,70,30);
	    timpMin.setVisible(true);
	    f.add(timpMin);
	    
	    
	    timpMaximProcesare=new JLabel("Timp maxim:");
	    timpMaximProcesare.setBounds(860,180,80,30);
	    timpMaximProcesare.setVisible(true);
	    f.add(timpMaximProcesare);
	    
	   timpMax=new JTextField();
	    timpMax.setBounds(860,220,70,30);
	    timpMax.setVisible(true);
	    f.add(timpMax);
	    
	    nrServeri=new JLabel("Nr serveri:");
	    nrServeri.setBounds(860,260,70,30);
	    nrServeri.setVisible(true);
	    f.add(nrServeri);
	    
	     nrSer=new JTextField();
	    nrSer.setBounds(860,300,70,30);
	    nrSer.setVisible(true);
	    f.add(nrSer);
	    
	    nrClienti=new JLabel("Nr clienti:");
	    nrClienti.setBounds(860,340,70,30);
	    nrClienti.setVisible(true);
	    f.add(nrClienti);
	   
	  nrCL=new JTextField();
	  nrCL.setBounds(860,380,70,30);
	  nrCL.setVisible(true);
	  f.add(nrCL);
	    
	    
	    
	 // scrollPane.createHorizontalScrollBar();
	 // f.getContentPane(). add(scrollPane);
	   //scrollPane.setVisible(true);
	
	}  
	 public String getTim() {
	    	return limTimp.getText();
	    }
	public void actionPerformed(ActionEvent e){  
		int tim=Integer.parseInt(limTimp.getText());
		int nrc=Integer.parseInt(nrCL.getText());
		int tmin=Integer.parseInt(timpMin.getText());
		int tmax=Integer.parseInt(timpMax.getText());
		int nrs=Integer.parseInt(nrSer.getText());
		  SimulationManager gen=new SimulationManager(this,tim,tmin,tmax,nrs,nrc);
	      Thread t=new Thread(gen);
	       gen.generateNRandomTasks();
	      // gen.run();
	       t.start();
	}  
	
}

